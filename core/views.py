import re
from django.shortcuts import render

from core.models import CustomUser

# Create your views here.

def login_view(request):
    template_name = 'core/login.html'
    context = {}
    return render(request, template_name, context)

def registration_view(request):
    template_name = 'core/register.html'
    context = {}
    return render(request, template_name, context)