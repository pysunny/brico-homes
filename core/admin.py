from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

from core.models import CustomUser


@admin.register(CustomUser)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields':('email','password')}),
        (_('Personal Info'), {'fields':('name',)}),
        (_('Permissions'), {'fields':('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important Dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes':('wide',),
            'fields': ('email','name','password1', 'password2'),
        }),
    )

    list_display = ('email','name','is_staff')
    search_field = ('email','name','groups','organization')
    ordering = ('email',)