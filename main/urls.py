"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from main.views import index_view, company_view, expert_view, join_view, project_view, sustainability_view, \
                        get_quotation_view, completed_project_view, upcomming_project_view, \
                        current_project_view, project_details_view, consult_now_view, home_design_view, \
                        smart_house_view, material_page_view, architecture_page_view, urban_view, rural_view, \
                        architecture_inner_view, architecture_inner_detail_view

from main.pages import architecture_kidbedroom_view, architecture_bathroom_view, architecture_diningroom_view, \
                        architecture_kitchen_view, architecture_masterbedroom_view

urlpatterns = [
    path('', index_view, name="index_view"),
    path('company/', company_view, name="company"),
    path('expert/', expert_view, name="expert"),
    path('join/', join_view, name="join"),
    path('project/', project_view, name="project"),
    path('bricko-homes/completed/', completed_project_view, name='completed'),
    path('bricko-homes/upcoming/', upcomming_project_view, name='upcoming'),
    path('bricko-homes/current/', current_project_view, name='current'),
    path('project/project-details/', project_details_view, name='project-details'),
    path('sustainability/', sustainability_view, name="sustainability"),
    path('get-quotation/', get_quotation_view, name='get-quotation'),
    path('bricko-homes/consult-now/', consult_now_view, name='consult-now'),
    path('bricko-homes/home-design/', home_design_view, name='home-design'),
    path('bricko-homes/material-page/', material_page_view, name='material-page'),
    path('bricko-homes/smart-house-page/', smart_house_view, name='smart-house-page'),
    path('bricko-homes/architecture-page/', architecture_page_view, name='architecture-page'),
    path('bricko-homes/architecture/design-inner/', architecture_inner_view, name="design-inner"),
    path('bricko-homes/architecture/design-inner/details/', architecture_inner_detail_view, name='design-inner-details'),
    path('bricko-homes/city/patna-urban/', urban_view, name='patna-urban'),
    path('bricko-homes/city/patna/patna-rural/', rural_view, name='patna-rural'),

    path('bricko-homes/page/bathroom-design/', architecture_bathroom_view, name="bathroom-design"),
    path('bricko-homes/page/kid-bedroom-design/', architecture_kidbedroom_view, name="kid-bedroom-design"),
    path('bricko-homes/page/master-bedroom-design/', architecture_masterbedroom_view, name="master-bedroom-design"),
    path('bricko-homes/page/kitchen-design/', architecture_kitchen_view, name="kitchen-design"),
    path('bricko-homes/page/dining-room-design/', architecture_diningroom_view, name="dining-room-design"),

]
