from django.shortcuts import redirect, render


def architecture_kitchen_view(request):
    template_name= 'architecture/kitchen.html'
    context = {}
    return render(request, template_name, context)

def architecture_masterbedroom_view(request):
    template_name= 'architecture/master-bedroom.html'
    context = {}
    return render(request, template_name, context)

def architecture_kidbedroom_view(request):
    template_name= 'architecture/kid-bedroom.html'
    context = {}
    return render(request, template_name, context)

def architecture_diningroom_view(request):
    template_name= 'architecture/dining-room.html'
    context = {}
    return render(request, template_name, context)

def architecture_bathroom_view(request):
    template_name= 'architecture/bathroom.html'
    context = {}
    return render(request, template_name, context)