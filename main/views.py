from email import message
from django.core.exceptions import RequestAborted
from django.db import models
from django.shortcuts import redirect, render
from main.models import ConsultNowModel, CostEstimationModel, CarousalModel, ProjectModel, TeamModel, BodyModel, FooterModel, ContactModel
# Create your views here.

def base_view(request):
    footer = FooterModel.objects.get(id=1)
    template_name='base.html'
    context = {
        'footer': footer
    }
    return render(request, template_name, context)

def index_view(request):
    template_name='index.html'
    crousal = CarousalModel.objects.filter(status=True)
    team = TeamModel.objects.all()[:3]
    body_1 = BodyModel.objects.get(id=1)
    footer = FooterModel.objects.get(id=1)
    context = {
        'crousals':crousal,
        'teams':team,
        'body': body_1,
        'footer': footer
    }
    return render(request, template_name, context)

def company_view(request):
    team = TeamModel.objects.all()[:3]
    template_name='main/company.html'
    context = {
        'teams' : team
    }
    return render(request, template_name, context)

def expert_view(request):
    team = TeamModel.objects.all()
    template_name='main/expert.html'
    context = {
        'teams' : team
    }
    return render(request, template_name, context)


def project_view(request):
    template_name='main/project.html'
    context = {}
    return render(request, template_name, context)

def completed_project_view(request):
    template_name='main/completed-proj.html'
    completed_project = ProjectModel.objects.filter(status='COMPLETED')
    context = {
        'completed_project': completed_project
    }
    return render(request, template_name, context)

def upcomming_project_view(request):
    template_name='main/upcoming-proj.html'
    context = {}
    return render(request, template_name, context)

def current_project_view(request):
    template_name='main/current-proj.html'
    context = {}
    return render(request, template_name, context)

def project_details_view(request):
    template_name='main/project-details.html'
    context = {}
    return render(request, template_name, context)



def sustainability_view(request):
    template_name='main/sustainability.html'
    context = {}
    return render(request, template_name, context)

def get_quotation_view(request):
    template_name='est-cost.html'
    context = {}
    if request.POST:
        cost_model = CostEstimationModel()
        cost_model.name = request.POST['_name']
        cost_model.email = request.POST['_email']
        cost_model.mobile_number = request.POST['_mobile']
        cost_model.plot_location = request.POST['_plot_location']
        cost_model.plot_bredth = request.POST['_plot_breadth']
        cost_model.plot_length = request.POST['_plot_length']
        cost_model.floor = request.POST['floors']
        cost_model.save()
        context = {
            'message':'Thank You! Your requirement has been sent to our engineers, They will get back to you shortly on provided details'
        }
        return render(request, template_name, context)
    return render(request, template_name, context)

def join_view(request):
    template_name='main/join.html'
    context = {}
    if request.POST:
        contact = ContactModel()
        contact.name = request.POST['name']
        contact.email = request.POST['email']
        contact.mobile = request.POST['number']
        contact.designation = request.POST['designation']
        contact.address = request.POST['address']
        contact.message = request.POST['message']
        
        contact.save()
        context = {
            'message':'You have submit your application sucessfully!!!'
        }
        return render(request, template_name, context)
    return render(request, template_name, context)

def consult_now_view(request):
    template_name='main/consult-now.html'
    if request.POST:
        consult_now = ConsultNowModel()
        consult_now.email = request.POST.get('email')
        consult_now.name = request.POST.get('name')
        consult_now.mobile = request.POST.get('mobile')
        consult_now.message = request.POST.get('message')
        consult_now.save()
        context = {
            'message' : 'Thank you for your Interest, You can expect a call from our designer soon!!!',
        }
        return render(request, template_name, context)

    context = {
            'message' : 'Fill the below form to get a call from our designer.',
        }
    return render(request, template_name, context)

def home_design_view(request):
    template_name = 'main/home-design.html'
    context = {}
    return render(request, template_name, context)

def material_page_view(request):
    template_name = 'main/material-page.html'
    context = {}
    return render(request, template_name, context)

def smart_house_view(request):
    template_name = 'main/smarthouse.html'
    context = {}
    return render(request, template_name, context)

def architecture_page_view(request):
    template_name = 'main/architecture-page.html'
    context = {}
    return render(request, template_name, context)

def architecture_inner_view(request):
    template_name = 'architecture/design-inner.html'
    context = {}
    return render(request, template_name, context)

def architecture_inner_detail_view(request):
    template_name = 'architecture/inner-detail-page.html'
    context = {}
    return render(request, template_name, context)


def urban_view(request):
    template_name='patna-urban.html'
    context = {}
    return render(request, template_name, context)
    
def rural_view(request):
    template_name='patna-rural.html'
    context = {}
    return render(request, template_name, context)
# def corusal_view(request):
#     template_name = 'main/slider.html'
#     crousal = CarousalModel.objects.filter(status=True)
#     context = {
#         'crousals':crousal
#     }
#     print(context)
#     return render(request, template_name, context)