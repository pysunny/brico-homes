from django.db import models
from django.db.models.fields import DurationField, EmailField
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
class CarousalModel(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    descrption = models.TextField(null=True, blank=True)
    url = models.URLField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)
    created_by = models.CharField(max_length=100, null=True, blank=True)
    status = models.BooleanField(default=True)
    def __str__(self):
        return self.name

class ProjectImageModel(models.Model):
    image_url = models.URLField(null=True, blank=True)
    name = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.name

class ProjectModel(models.Model):
    STAUTS_CHOICE = (
        ('CURRENT', 'CURRENT'),
        ('UPCOMING','UPCOMING'),
        ('COMPLETED', 'COMPLETED'),
    )
    title = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    content = RichTextUploadingField(blank=True, null=True)
    status = models.CharField(max_length=15, choices=STAUTS_CHOICE, default='UPCOMING')
    duration = models.DurationField(null=True, blank=True)
    location = models.TextField(null=True, blank=True)
    contact_person = models.TextField(null=True, blank=True)
    mobile_number = models.BigIntegerField(blank=True)
    images = models.ManyToManyField(ProjectImageModel, blank=True)
    plot_size = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.title


class TeamModel(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    description = RichTextUploadingField(blank=True, null=True)
    profile_dp = models.ImageField( upload_to="teams/profile/", null=True, blank=True, db_index=True)
    designation = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    email = models.EmailField(null=True, blank=True)
    mobile_number = models.BigIntegerField(null=True, blank=True)
    year_of_experience = models.IntegerField(blank=True)

    def __str__(self) -> str:
        return self.name
    
class CostEstimationModel(models.Model):
    email = models.EmailField(blank=True, null=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    mobile_number = models.BigIntegerField(blank=True, null=True)
    plot_location = models.CharField(max_length=500, null=True, blank=True)
    plot_bredth = models.IntegerField(blank=True, null=True)
    plot_length = models.IntegerField(blank=True, null=True)
    floor = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self) -> str:
        return self.email

class FooterModel(models.Model):
    address = models.TextField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    contact_number = models.BigIntegerField(blank=True)
    privacy = RichTextUploadingField(blank=True, null=True)
    term_use = RichTextUploadingField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)
    
    def __str__(self) -> str:
        return self.address


class CareerModel(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    designation = models.CharField(max_length=100, null=True, blank=True)
    mobile_number = models.BigIntegerField(blank=True)
    address = models.TextField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name

class SustainabilityModel(models.Model):
    title = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField(null=True, blank=True)
    content = RichTextUploadingField(blank=True, null=True)
    image = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    created_by = models.CharField(max_length=100, default='default-user')

    def __str__(self) -> str:
        return self.title

class SiteModel(models.Model):
    company_name = models.CharField(max_length=100, blank=True, null=True)
    company_logo = models.URLField(blank=True, null=True)
    company_icon = models.URLField(blank=True, null=True)
    about_us = models.CharField(max_length=50, default='About Our Commpany')
    about_content = models.TextField(null=True, blank=True)
    our_team = models.CharField(max_length=50, default='Our Team Details')
    team = models.ManyToManyField(TeamModel, blank=True)

    def __str__(self):
        return self.company_name

class BodyModel(models.Model):
    section_title = models.CharField(max_length=100, null=True, blank=True)
    section_content = RichTextUploadingField(blank=True, null=True)
    image = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    created_by = models.CharField(max_length=50, default='default')

    def __str__(self) -> str:
        return self.section_title

class NewsSliderModel(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    bg_img1 = models.URLField(null=True, blank=True)
    bg_img2 = models.URLField(null=True, blank=True)
    link1 = models.URLField(null=True, blank=True)
    link2 = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.title

class EventImageModel(models.Model):
    name=models.CharField(max_length=100, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    
    def __str__(self) -> str:
        return self.name
class EventModel(models.Model):
    name = models.CharField(max_length=500, null=True, blank=True)
    images = models.ManyToManyField(EventImageModel, blank=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name

class PackageModel(models.Model):
    PACKAGE_CHOICE = (
        ('BASIC','BASIC'),
        ('STANDARD', 'STANDARD'),
        ('PREMIUM', 'PREMIUM'),
    )
    name = models.CharField(max_length=100, choices=PACKAGE_CHOICE, default='BASIC')
    price = models.IntegerField(blank=True)
    created_at = models.DateTimeField(auto_now=True)
    created_by = models.CharField(max_length=50, default='default')

    def __str__(self) -> str:
        return self.name

class PackageItemListModel(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.name
class PackageItemModel(models.Model):
    package_id = models.ForeignKey(PackageModel, on_delete=models.CASCADE, null=True, blank=True)
    item = models.CharField(max_length=100, null=True, blank=True)
    item_list = models.ManyToManyField(PackageItemListModel, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50, default='default')

    def __str__(self) -> str:
        return self.item

class ConsultNowModel(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    mobile = models.BigIntegerField(blank=True)
    message = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name

class ContactModel(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    mobile = models.BigIntegerField(blank=True)
    designation = models.CharField(max_length=100, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name

class CityModel(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    content = RichTextUploadingField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50, help_text="User name", default='anonymus')

    def __str__(self) -> str:
        return self.name
