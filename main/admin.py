from csv import list_dialects
import email
from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from main.models import CostEstimationModel, CarousalModel, \
                        ProjectImageModel, ProjectModel, TeamModel, FooterModel, CareerModel, \
                        SustainabilityModel, SiteModel, BodyModel, EventImageModel, PackageModel, \
                        PackageItemListModel, PackageItemModel, NewsSliderModel, ContactModel, CityModel, ConsultNowModel
# Register your models here.

@admin.register(CostEstimationModel)
class CostEstimationModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'email', 'name', 'mobile_number')
    search_fields = ("name__startswith", )

@admin.register(CarousalModel)
class CarousalModel(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name', 'status')
    search_fields = ('name__startswith', )

@admin.register(ProjectImageModel)
class ProjectImageModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name', 'image_url')

@admin.register(ProjectModel)
class ProjectModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('title', 'status')
    search_fields = ('title__startswith', )

@admin.register(TeamModel)
class TeamModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('name', 'designation', 'email', 'mobile_number')
    search_fields = ('name__startswith', )

@admin.register(SiteModel)
class SiteModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('company_name',)

@admin.register(FooterModel)
class FooterModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('email', 'address', 'contact_number')

@admin.register(CareerModel)
class CareerModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('name', 'email', 'mobile_number', 'designation')
    search_fields = ('name__startswith', )

@admin.register(SustainabilityModel)
class SustainabilityModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'title', 'description')

@admin.register(BodyModel)
class BodyModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'section_title')

@admin.register(EventImageModel)
class EventImageModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name', 'url')
    search_fields = ('name__startswith', )

@admin.register(PackageModel)
class PackageModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name',)


@admin.register(PackageItemListModel)
class PackageItemListModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name',)
    search_fields = ('name__startswith', )


@admin.register(PackageItemModel)
class PackageItemModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'item',)
    search_fields = ('item__startswith', )


@admin.register(NewsSliderModel)
class NewsSliderModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'title')


@admin.register(ContactModel)
class ContactModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name', 'designation')

@admin.register(CityModel)
class CityModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name','created_at', 'created_by')

@admin.register(ConsultNowModel)
class ConsultNowModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'email', 'mobile', 'message')